-> Designed and implemented a custom data structure named ‘myArraylist’ which is my own implementation of the java.util.ArrayList class .

-> It has all the functionality and methods similar to that of the standard ArrayList API.

-> It is designed using an array with the feature of dynamic resizing. Kept track of the number of items currenly in the array and once the 
   array size reached its capacity, the size of the array is increased to 1.5 * current_array_size. The old array content is then copied into the 
   same slots of the newly created array and subsequently any new element is added into the new array.

---TIME COMPLEXITY--- 
- Takes constant time for Insertion. 
- Sorting takes O(n*n) time. 
- Searching for an element takes linear time . 
- removeVal() which is used for deleting an element takes Linear time as moving of elements is involved.