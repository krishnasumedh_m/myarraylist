package myArrayList;

import java.util.AbstractList;

public class MyArrayList <Integer> extends AbstractList <Integer>
{
    private int[] array;
    private int[] arr;
    int index ;
    int initsize = 4;
    int count = 0 ;
    public int flag = 0;
    
    public MyArrayList()
    {  
       index = 0;
       array = new int[initsize]; 
    }	
    
    public void insertSorted(int newValue)
    {    
    	int j; 
    	if( (newValue< 0) || (newValue> 10000) )
    	{
    		System.err.println("Element not present in range. Cannot Insert");   
    		System.exit(1);
    	}	
        
    
       if ( index >= initsize )
        {    arr = new int [initsize + (initsize)/2 ]; //allocate new array
             for(j=0; j < initsize; j++)
             {     arr[j] = array[j];        
             } 
              array  = arr;
              initsize = initsize + (initsize)/2 ;
        }                
            
            array[index] = newValue ; 
            count ++;
            
            // Arrays.sort(array);
            //Do the Sorting after every insertion.
            for(int i=0; i <= index; i++)
            {
                 int min = i;

                 for (int x = i+1; x <= index; x++)
                 {
                   if ( array[min] > array[x]  )
                        min = x;
                 }
           
            int temp = array[min];
            array[min] = array[i];
            array[i] = temp;                              
            }  
                
            index ++; // increment index for next insertion
    }   
    
    public void removeValue(int value)
    {
      flag = -1;
      
     if(count > 0)
     {	 
    	 flag = 0;
         for(int i=0;i<count;)
         {
            if (array[i] == value)
           {   
              int k = i;
              for(int y =k+1; y < index; y++ )
              {  array[k] = array[y];
                  k++;
              }
            index = index - 1;
            count = count -1;
            flag = 1;
            
          }
            else
               i++;    
          }
          if(flag == 0)
          {  System.out.println("\n Error!: Element not present in list. Cannot be removed \n"); 
             //System.err.flush();
          } 
     }
     
     if(count == 0 && flag== -1 )
     {    
    	 System.out.println("\n Error!: List Empty:Remove operation not valid \n"); 
    	 //System.err.flush();
     }
      
    }
    
    public int indexOf(int value)
    {
        for(int i=0;i<count;i++)
       {
           if(array[i] == value)
           {
             return i;  
           }    
       }
    return -1;   //not present 
    }
    
    @Override
    public int size()
    {
     //System.out.println("The Size of the array  " + count);    
     return count;   
    }
    
    
    public int sum()
    { 
      
       int sum=0;
       for(int i=0;i<count;i++)
       {
           sum = sum + array[i];
       }
       return sum;
    }
    
    
    @Override
    public  String toString() //print all the elements in the array
    {  
       //System.out.println("The Elements stored in the array");
       String str = "";
       for(int i=0;i<count;i++)
       {
          //System.out.println(array[i]);
          str = str +" "+array[i] ;
       }
     
     return str;
    }
    
    
    public void reset()
    {
      for(int i=0;i<count;i++)
       {
           array[i] = 0 ;
       }
      
      count = 0;
      index = 0;
    }

    
    public boolean compare(int[] iarr)
    {
    	for(int i=0;i<iarr.length;i++)
        {
           //System.out.println(iarr[i]);
           if(iarr[i] == array[i] )
             {        
        	 }
           else
        	   return false;
        }
    	return true;   	
    }
    
    
    
    @Override
    public Integer get(int index) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
        
        
}
    
    

