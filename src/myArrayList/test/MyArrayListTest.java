package myArrayList.test;

import myArrayList.util.FileProcessor;
import myArrayList.util.Results;
import  myArrayList.MyArrayList;

public class MyArrayListTest 
{
     String line = null;
     int value;
    
    public void read(FileProcessor fp, MyArrayList arl)
    {       
     while( null != (line = fp.readLine()) )
     {
        try
	{
	value = Integer.parseInt(line);
	}
	catch (NumberFormatException e)
	{
	System.exit(1);
	}
	arl.insertSorted(value);   
     }
    
   }
    
    
     public void testMe(MyArrayList myArrayList, Results results )
     {  
        test1(myArrayList, results);
        
        test2(myArrayList, results);
        
        test3(myArrayList, results);
        
        test4(myArrayList, results);
        
        test5(myArrayList, results);
        
        test6(myArrayList,  results);
        
        test7(myArrayList,  results);
        
        test8(myArrayList,  results);
        
        test9(myArrayList,  results);
        
        test10(myArrayList,  results);
        
     
     }
    
void test1(MyArrayList myArrayList, Results results)
{      
      //Test:Sum of all elements in the arrayList
	  //System.out.println("Test Case: 1");
	  myArrayList.reset();
      int sum = 0;
      myArrayList.insertSorted(10);
      myArrayList.insertSorted(20);
      myArrayList.insertSorted(50);
      
      sum = myArrayList.sum();
      if (sum == 80)
      {   
          results.storeNewResult("\nTest: Sum of two elements -> "+ "Test Passed");
      }
      else
      {
          results.storeNewResult("\nTest: Sum of two elements -> "+ "Test Failed");      
      }        
}
    
void test2(MyArrayList myArrayList, Results results)
{
	//Test:Remove duplicate elements
	
	String str = " 20 40"; //Expected correct result 
	myArrayList.reset();
	myArrayList.insertSorted(40);
	myArrayList.insertSorted(10);
    myArrayList.insertSorted(20);
    myArrayList.insertSorted(10);
   // myArrayList.size();
    myArrayList.removeValue(10);
  //  myArrayList.toString();
   // myArrayList.size();
    if((myArrayList.size() == 2) && str.equals(myArrayList.toString()))
    {   
        results.storeNewResult("\nTest: Remove duplicate elements -> "+ "Test Passed");
    }
    else
    {
        results.storeNewResult("\nTest: Remove duplicate elements -> "+ "Test Failed");
    
    }    
    	    
}


void test3(MyArrayList myArrayList, Results results)
{ 
	//Test: Exceed array capacity
	
	int[] iarr = new int[11];
    myArrayList.reset();
    for(int i=11;i>0;i--)
    {  
    	myArrayList.insertSorted(i) ;
       
        iarr[i-1] = i;
    	
    }
    myArrayList.toString(); //
    myArrayList.size();    //
    
    
    if( (myArrayList.compare(iarr)) == true)
    {   
        results.storeNewResult("\nTest: Insert-Exceed array capacity -> "+ "Test Passed");
    }
    else
    {
        results.storeNewResult("\nTest: Insert-Exceed array capacity -> "+ "Test Failed");
    
    }     	
    
    
}
    
void test4(MyArrayList myArrayList, Results results)
{
	//get index of an element not present
	
    myArrayList.reset();
    int result = myArrayList.indexOf(5);
    if(result == -1)
    	results.storeNewResult("\nTest: Retreive index of an NonExisting element -> "+ "Test Passed");
    else
    	results.storeNewResult("\nTest: Retreive index of an NonExisting element -> "+ "Test Failed");
    	
    
}
    
  void test5(MyArrayList myArrayList, Results results)
{     
	  //get index of an element's first Occurrence
	  
	  myArrayList.reset();
	  myArrayList.insertSorted(20);
	  myArrayList.insertSorted(20);
	  if( myArrayList.indexOf(20) == 0)
		  results.storeNewResult("\nTest: get index of an element's first Occurance -> "+ "Test Passed");
	    else
	    	results.storeNewResult("\nTest: get index of an element's first Occurance -> "+ "Test Failed");
	 
}   
     
  
  void test6(MyArrayList myArrayList, Results results)
  { 
	  //remove element from an empty list
	  System.out.println("Test Case -> 6 : remove element from an empty list");
  	  myArrayList.reset();
  	  String result = "" ;
  	  myArrayList.removeValue(10);
      String a = myArrayList.toString(); //
      if(a.equals(result))
    	  results.storeNewResult("\nTest: Remove element from empty List -> "+ "Test Passed");     
      else
    	  results.storeNewResult("\nTest: Remove element from empty List"+ "Test Failed");
      
  }  
  
  void test7(MyArrayList myArrayList, Results results)
  {   
	  //Check the size of an empty array
	  
  	  myArrayList.reset();
  	 if( myArrayList.size() == 0 )
  		  results.storeNewResult("\nTest: Check the size of an empty arrayList -> "+ "Test Passed");     
      else
    	  results.storeNewResult("\nTest: Check the size of an empty arrayList -> "+ "Test Passed");
  		  
  		  
  	  
  }   
  
  void test8(MyArrayList myArrayList, Results results)
  { 
	  //delete an non existent element
	  System.out.println("Test Case -> 8 : delete an non existent element");
  	  myArrayList.reset();
  	  myArrayList.insertSorted(4);
  	  myArrayList.removeValue(4);
  	// System.out.println("flag " + myArrayList.flag);
  	  myArrayList.insertSorted(4);
	  myArrayList.removeValue(3);
	//System.out.println("flag " + myArrayList.flag);
	  if(myArrayList.flag == 0)
		  results.storeNewResult("\nTest: delete an non existent element -> "+ "Test Passed");     
      else
    	  results.storeNewResult("\nTest: delete an non existent element -> "+ "Test Failed");
  	  
  	  
  }   
  
  void test9(MyArrayList myArrayList, Results results)
  {  
	  //Sum of an empty Array List
	  //System.out.println("Test Case: 9");
  	  myArrayList.reset();
  	  if(myArrayList.sum() == 0)
  		results.storeNewResult("\nTest: Sum of an empty Array List -> "+ "Test Passed");     
      else
    	  results.storeNewResult("\nTest: Sum of an empty Array List -> "+ "Test Failed");
  		  
  	  
  	  
  }   
  
  void test10(MyArrayList myArrayList, Results results)
  { 
	  //Test for insertion and removal
	 // System.out.println("Test Case ->10");
	 String Str = " 40 41 43"; //Expected correct result 40 41 43
  	 myArrayList.reset();
     myArrayList.insertSorted(43);
  	 myArrayList.insertSorted(40);
  	 myArrayList.insertSorted(42);
  	 myArrayList.insertSorted(41);
  	 myArrayList.insertSorted(45);
  	 
 	  myArrayList.removeValue(42);
 	  myArrayList.removeValue(45);
 	 String s = myArrayList.toString();
 	 if(Str.equals(s))
 		results.storeNewResult("\nTest: Test for insertion and removal -> "+ "Test Passed");     
     else
   	   results.storeNewResult("\nTest: Test for insertion and removal -> "+ "Test Failed");
 	  
  	  
  }   
  

     
}   

