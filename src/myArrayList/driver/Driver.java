package myArrayList.driver;

import myArrayList.util.FileProcessor;
import myArrayList.util.Results;
import myArrayList.test.MyArrayListTest;
import myArrayList.MyArrayList;

public class Driver 
{
   String line;
   static String ofname=null;
    
   public static void main(String[] args)
   {     
	   
	   if( (args.length != 2))
	   {
		   System.err.println("Invalid Arguments! Format : <input.txt> <output.txt> \n");
		   System.exit(1);
	   }
	   
         String str = null ;
         ofname = args[1];
	   //FileProcessor fp = new FileProcessor("input.txt");
         FileProcessor fp = new FileProcessor(args[0]);
    MyArrayList <Integer> arl = new  MyArrayList <Integer>();
    arl.reset();
    Results result = new Results();
    MyArrayListTest test = new MyArrayListTest();
    test.read(fp, arl);
    String sum = "The Sum of all values in the arrayList is " + arl.sum();
    result.storeNewResult(sum);
    
    str = arl.toString();
    System.out.println("The Elements stored in the array");
    System.out.println(" " + str);
    
    test.testMe(arl,result);
    
    result.writeToStdout(result.getResult());
    //System.out.println(result.getResult());
    result.writeToFile(ofname);
    
}
   
}

