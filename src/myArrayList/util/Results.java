package myArrayList.util;

import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

 
public  class Results implements FileDisplayInterface,StdoutDisplayInterface
{
    
    private ArrayList<String> ar = new ArrayList<String>(); 
    BufferedWriter bwrite= null;
    FileWriter fwrite = null;
    String result = "";
    
    public void storeNewResult(String s)
    {  //insert result strings into the arrayList
       ar.add(s);
    }
    
    public String getResult()
    {  
    	     
    	for (String res: ar)
    	{
            result = result + res + "    ";
        }
    	
    	return result;
    }
    
    @Override
    public void writeToFile(String s)
    {
    
    //contains the sum of the integers given in the input file along with the "test XYZ passed" string test condition strings
    try
    {
    	fwrite = new FileWriter(s);
    	bwrite = new BufferedWriter (fwrite);
    	bwrite.write(result);
    }
    catch (IOException io) 
    {   System.err.println("error : Output file");
		io.printStackTrace();
	} 
    
    finally 
    {
		try 
		{
			if (bwrite != null)
				bwrite.close();

			if (fwrite != null)
				fwrite.close();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
	    }
    }
    
    	
    }

    @Override
    public void writeToStdout(String s)
    {
    	 System.out.println(s);
    
    }


}



