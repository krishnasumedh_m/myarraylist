package myArrayList.util;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileProcessor 
{
    String file = null;
    BufferedReader br;
    String line;
    
    public FileProcessor (String fname)
    {
       file = fname; 
       try
       {
         FileReader fr = new FileReader(file);
         br = new BufferedReader(fr);
       }
       
      catch(FileNotFoundException f)
      {
          System.err.println("File Not Found");
          System.exit(1);
      }
    }
  
    public  String readLine()
    { 
      try
      {    
        line = br.readLine();
      }
      catch(IOException exc)
      {
    	  exc.printStackTrace();
      }   
      return line;
    }
}
